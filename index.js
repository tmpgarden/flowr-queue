var Hose = require('flowr-hose');

var args = {
  name: 'flowr-queue',
  port: 0,
  kue: {
    prefix: 'q',
    redis: process.env.KUE || 'redis://localhost:6379'
  }
};

args.ui = {
	port: process.env.KUE_UI_PORT || 3000
};

var hose = new Hose(args);
